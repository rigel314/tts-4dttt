state = {}

function onLoad(save_state)
    for x = 1, 5 do
        state[x] = {}
        for z = 1, 5 do
            state[x][z] = {}
            for y = 1, 5 do
                state[x][z][y] = {}
                for w = 1, 5 do state[x][z][y][w] = nil end
            end
        end
    end
    if save_state ~= '' then state = JSON.decode(save_state) end
end

function onSave() return JSON.encode(state) end

function onScriptingButtonDown(index, player_color)
    if index == 10 and Player[player_color].admin then
        print('state dump follows')
        for y = 5, 1, -1 do
            for z = 5, 1, -1 do
                str = ''
                for w = 1, 5 do
                    for x = 1, 5 do
                        objg = state[x][z][y][w]
                        if objg == nil then
                            str = str .. '-'
                        else
                            obj = getObjectFromGUID(objg)
                            color = obj.getVar('color')
                            if color == 'White' then
                                str = str .. 'w'
                            elseif color == 'Red' then
                                str = str .. 'r'
                            elseif color == 'Blue' then
                                str = str .. 'b'
                            else
                                str = str .. '?'
                            end
                        end
                    end
                    str = str .. '    '
                end
                print(str)
            end
            print('')
        end
    end
end

function onObjectEnterScriptingZone(zone, obj)
    obj_pos = obj.getPosition()
    color = obj.getVar('color')
    if color == nil then return end
    if obj.call('isHeld', {}) then return end

    y = zone.getVar('y')
    w = zone.getVar('w')

    zone_pos = zone.getPosition()
    xdiff = obj_pos['x'] - zone_pos['x']
    zdiff = obj_pos['z'] - zone_pos['z']

    -- range is roughly -2.7 to +2.7
    -- scaling that to [0,4] is (diff + 2.7)/1.08
    x = math.floor(math.abs((xdiff + 2.7) / 1.08))
    z = math.floor(math.abs((zdiff + 2.7) / 1.08))

    if x < 0 then x = 0 end
    if x > 4 then x = 4 end
    if z < 0 then z = 0 end
    if z > 4 then z = 4 end

    x = x + 1
    z = z + 1

    state[x][z][y][w] = obj.getGUID()
    if checkWin(x, z, y, w) then
        name = ''
        if Player[color].seated then name = Player[color].steam_name end
        printToAll(name .. '(' .. color .. ') wins!', {r = 0, g = 1, b = 1})
    end
end

function onObjectLeaveScriptingZone(zone, obj)
    y = zone.getVar('y')
    w = zone.getVar('w')

    for x = 1, 5 do
        for z = 1, 5 do
            if obj.getGUID() == state[x][z][y][w] then
                state[x][z][y][w] = nil
            end
        end
    end
end

function checkLine(start, dir)
    objg = state[start[1]][start[2]][start[3]][start[4]]
    if objg == nil then return false end
    obj = getObjectFromGUID(objg)
    if obj == nil then return false end
    val = obj.getVar('color')
    if val == nil then return false end
    for i = 1, 4 do
        start[1] = start[1] + dir[1]
        start[2] = start[2] + dir[2]
        start[3] = start[3] + dir[3]
        start[4] = start[4] + dir[4]
        otherobjg = state[start[1]][start[2]][start[3]][start[4]]
        if otherobjg == nil then return false end
        otherobj = getObjectFromGUID(otherobjg)
        if otherobj == nil then return false end
        if otherobj.getVar('color') ~= val then return false end
    end

    return true
end

function checkWin(x, z, y, w)
    M = 5
    L = 1

    if checkLine({L, z, y, w}, {1, 0, 0, 0}) then return true end
    if checkLine({x, L, y, w}, {0, 1, 0, 0}) then return true end
    if checkLine({x, z, L, w}, {0, 0, 1, 0}) then return true end
    if checkLine({x, z, y, L}, {0, 0, 0, 1}) then return true end

    if x == z then
        if checkLine({L, L, y, w}, {1, 1, 0, 0}) then return true end
    end
    if x == M - z then
        if checkLine({L, M, y, w}, {1, -1, 0, 0}) then return true end
    end

    if x == y then
        if checkLine({L, z, L, w}, {1, 0, 1, 0}) then return true end
    end
    if x == M - y then
        if checkLine({L, z, M, w}, {1, 0, -1, 0}) then return true end
    end

    if x == w then
        if checkLine({L, z, y, L}, {1, 0, 0, 1}) then return true end
    end
    if x == M - w then
        if checkLine({L, z, y, M}, {1, 0, 0, -1}) then return true end
    end

    if z == y then
        if checkLine({x, L, L, w}, {0, 1, 1, 0}) then return true end
    end
    if z == M - y then
        if checkLine({x, L, M, w}, {0, 1, -1, 0}) then return true end
    end

    if z == w then
        if checkLine({x, L, y, L}, {0, 1, 0, 1}) then return true end
    end
    if z == M - w then
        if checkLine({x, L, y, M}, {0, 1, 0, -1}) then return true end
    end

    if y == w then
        if checkLine({x, z, L, L}, {0, 0, 1, 1}) then return true end
    end
    if y == M - w then
        if checkLine({x, z, L, M}, {0, 0, 1, -1}) then return true end
    end

    if x == z and x == y and z == y then
        if checkLine({L, L, L, w}, {1, 1, 1, 0}) then return true end
    end
    if x == M - z and x == y and M - z == y then
        if checkLine({L, M, L, w}, {1, -1, 1, 0}) then return true end
    end
    if x == z and x == M - y and z == M - y then
        if checkLine({L, L, M, w}, {1, 1, -1, 0}) then return true end
    end
    if x == M - z and x == M - y and M - z == M - y then
        if checkLine({L, M, M, w}, {1, -1, -1, 0}) then return true end
    end

    if x == z and x == w and z == w then
        if checkLine({L, L, y, L}, {1, 1, 0, 1}) then return true end
    end
    if x == M - z and x == w and M - z == w then
        if checkLine({L, M, y, L}, {1, -1, 0, 1}) then return true end
    end
    if x == z and x == M - w and z == M - w then
        if checkLine({L, L, y, M}, {1, 1, 0, -1}) then return true end
    end
    if x == M - z and x == M - w and M - z == M - w then
        if checkLine({L, M, y, M}, {1, -1, 0, -1}) then return true end
    end

    if x == y and x == w and y == w then
        if checkLine({L, z, L, L}, {1, 0, 1, 1}) then return true end
    end
    if x == M - y and x == w and M - y == w then
        if checkLine({L, z, M, L}, {1, 0, -1, 1}) then return true end
    end
    if x == y and x == M - w and y == M - w then
        if checkLine({L, z, L, M}, {1, 0, 1, -1}) then return true end
    end
    if x == M - y and x == M - w and M - y == M - w then
        if checkLine({L, z, M, M}, {1, 0, -1, -1}) then return true end
    end

    if z == y and z == w and y == w then
        if checkLine({x, L, L, L}, {0, 1, 1, 1}) then return true end
    end
    if z == M - y and z == w and M - y == w then
        if checkLine({x, L, M, L}, {0, 1, -1, 1}) then return true end
    end
    if z == y and z == M - w and y == M - w then
        if checkLine({x, L, L, M}, {0, 1, 1, -1}) then return true end
    end
    if z == M - y and z == M - w and M - y == M - w then
        if checkLine({x, L, M, M}, {0, 1, -1, -1}) then return true end
    end

    if x == z and x == y and x == w and z == y and z == w and y == w then
        if checkLine({L, L, L, L}, {1, 1, 1, 1}) then return true end
    end
    if x == z and x == y and x == M - w and z == y and z == M - w and y == M - w then
        if checkLine({L, L, L, M}, {1, 1, 1, -1}) then return true end
    end
    if x == z and x == M - y and x == w and z == M - y and z == w and M - y == w then
        if checkLine({L, L, M, L}, {1, 1, -1, 1}) then return true end
    end
    if x == M - z and x == y and x == w and M - z == y and M - z == w and y == w then
        if checkLine({L, M, L, L}, {1, -1, 1, 1}) then return true end
    end
    if x == z and x == M - y and x == M - w and z == M - y and z == M - w and M -
        y == M - w then
        if checkLine({L, L, M, M}, {1, 1, -1, -1}) then return true end
    end
    if x == M - z and x == M - y and x == w and M - z == M - y and M - z == w and
        M - y == w then
        if checkLine({L, M, M, L}, {1, -1, -1, 1}) then return true end
    end
    if x == M - z and x == y and x == M - w and M - z == y and M - z == M - w and
        y == M - w then
        if checkLine({L, M, L, M}, {1, -1, 1, -1}) then return true end
    end
    if x == M - z and x == M - y and x == M - w and M - z == M - y and M - z ==
        M - w and M - y == M - w then
        if checkLine({L, M, M, M}, {1, -1, -1, -1}) then return true end
    end

    return false
end
