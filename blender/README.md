blender
======

## Story
I originally had intended to import the entire board as one object.  This proved to have too many vertices for TTS, so it rejected it.  Then I made board_simple as a lowpoly version.  But that wouldn't let me mess with the opacity of the texture correctly.  I hadn't even tried a collision mesh.  It probably would have worked.

The higher res version is called layer, and is a single 5x5 board piece.  I was able to import it in unity, define a glass material, and export as an assetbundle using the example unity project.

I was confused about a convex vs non-convex collider.  I tried to make a collider mesh that was very very low res and came out to a 3-5 less than the max of 255 triangles.  But, in the end, I was able to mark the board pieces as fixed/locked and use the render mesh for collisions.  Though, I had to add an extra plane for the collision mesh which wouldn't let small objects fall through the holes.  Bigger objects that rested on the beveled edges would vibrate in TTS.  Smaller objects would fall through.  With the extra plane, smaller objects just sit still in the holes.

Everything else was layed out in-game or by editing the save file.

## Export settings
* FBX
* Scale: 50
* Selected Objects only
* Don't apply unit
