Tabletop Simulator 4D Tic Tac Toe
======

## About
There's a lot of parts that go into this.  I wanted to collect them all in case the steam workshop loses it all.  [Workshop link](https://steamcommunity.com/sharedfiles/filedetails/?id=2057938373)

## Subfolders
* blender
  * I used blender to make all of the meshes I used, exporting to fbx for import into unity
* unity
  * This is basically a copy of the example project from here: https://kb.tabletopsimulator.com/custom-content/custom-assetbundle/
  * I created a unityGlass material, and a setup to export a board peice prefab as an assetbundle in the format tabletop sim wants it
* tts
  * A save file for TableTop Simulator that is the starting point for the workshop content
  * A copy of the Global lua script, which is the only one that matters
* workshop
  * Contains images and text used as assets in the workshop
* go
  * Go programs I made for various reasons along the way, see inside for more details
