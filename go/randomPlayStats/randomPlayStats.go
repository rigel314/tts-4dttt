package main

import (
	"log"
	"math/rand"
	"runtime"
	"time"

	"gitlab.com/rigel314/tts-4dttt/go/lines"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	winCh := make(chan int, 2*runtime.NumCPU())

	for i := 0; i < 2*runtime.NumCPU(); i++ {
		go func() {
			plays := make([]int, 625)
			for i := 0; i < 625; i++ {
				plays[i] = i
			}
			b := new(board)
			idx := make([]int, 4)
			lns := make([]lines.Line, 40)
			for {
				game(plays, b, idx, lns, winCh)
			}
		}()
	}

	var p1win, p2win, tie uint64
	var gamecount uint64
	for v := range winCh {
		gamecount++
		switch v {
		case 0:
			tie++
		case 1:
			p1win++
		case 2:
			p2win++
		}
		if gamecount%100000 == 0 {
			log.Printf("total: %d, p1: %d, p2: %d, tie: %d\n", gamecount, p1win, p2win, tie)
		}
	}
}

type board [5][5][5][5]int

func integerdigits(num uint32, base int) []int {
	ret := make([]int, 0, 32)
	for ; num != 0; num /= uint32(base) {
		ret = append(ret, int(num%uint32(base)))
	}
	return ret
}

func integerdigits625_5(num int, idx []int) {
	idx[0] = num % 5
	num = num / 5
	idx[1] = num % 5
	num = num / 5
	idx[2] = num % 5
	num = num / 5
	idx[3] = num % 5
}

func (b board) String() (ret string) {
	ret += "\n"
	for y := 4; y >= 0; y-- {
		for z := 4; z >= 0; z-- {
			for w := 0; w < 5; w++ {
				for x := 0; x < 5; x++ {
					switch b[w][y][z][x] {
					case 0:
						ret += "."
					case 1:
						ret += "w"
					case 2:
						ret += "b"
					case 3:
						ret += "r"
					}
				}
				ret += "    "
			}
			if !(y == 0 && z == 0) {
				ret += "\n"
			}
		}
		if y != 0 {
			ret += "\n"
		}
	}
	return
}

func (b *board) checkLine(l lines.Line) bool {
	// s := make([]int, len(l.Start))
	// copy(s, l.Start)
	w, y, z, x := l.Start[3], l.Start[2], l.Start[1], l.Start[0]
	val := b[w][y][z][x]
	if val == 0 {
		return false
	}
	for i := 0; i < 4; i++ {
		w += l.Diff[3]
		y += l.Diff[2]
		z += l.Diff[1]
		x += l.Diff[0]
		if val != b[w][y][z][x] {
			return false
		}
	}

	return true
}

func (b *board) checkWin(p []int, lns []lines.Line) (lines.Line, bool) {
	for _, v := range lines.Lines4DPrealloc(p[0], p[1], p[2], p[3], lns) {
		if b.checkLine(v) {
			return v, true
		}
	}
	return lines.Line{}, false
}

func game(plays []int, b *board, idx []int, lns []lines.Line, winCh chan int) {
	// plays = rand.Perm(625)
	rand.Shuffle(625, func(i, j int) {
		plays[i], plays[j] = plays[j], plays[i]
	})
	// for w := 0; w < 5; w++ {
	// 	for y := 0; y < 5; y++ {
	// 		for z := 0; z < 5; z++ {
	// 			for x := 0; x < 5; x++ {
	// 				b[w][y][z][x] = 0
	// 			}
	// 		}
	// 	}
	// }
	b = new(board)
	for i := 0; i < 40; i++ {
		lns[i] = lines.Line{Start: []int{0, 0, 0, 0}, Diff: []int{0, 0, 0, 0}}
	}
	for i := 0; i < 625; i++ {
		// idx := integerdigits(uint32(plays[i]), 5)
		// for len(idx) < 4 {
		// 	idx = append(idx, 0)
		// }
		integerdigits625_5(plays[i], idx)
		val := i&1 + 1
		b[idx[3]][idx[2]][idx[1]][idx[0]] = val
		if _, win := b.checkWin(idx, lns); win {
			// log.Println(i, l, val, "wins!")
			winCh <- val
			return
		}
	}
	winCh <- 0
}
