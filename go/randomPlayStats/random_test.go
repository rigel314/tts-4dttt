package main

import (
	"math/rand"
	"testing"

	"gitlab.com/rigel314/tts-4dttt/go/lines"
)

func TestIntegerdigits(t *testing.T) {
	counter := []int{0, 0, 0, 0}
	for i := 0; i < 625; i++ {
		dig := integerdigits(uint32(i), 5)
		t.Log(i, ReverseInt(dig), ReverseInt(counter))
		if !CompareIntDigits(dig, counter) {
			t.Fail()
		}
		IntDigitInc(counter, 5)
	}
}

func ReverseInt(a []int) []int {
	ret := make([]int, len(a))
	for i := 0; i < len(a); i++ {
		ret[len(a)-1-i] = a[i]
	}
	return ret
}

func CompareIntDigits(a, b []int) bool {
	if len(a) != len(b) {
		if len(a) < len(b) && IntIsZeros(b[len(a):]) {
			b = b[:len(a)]
		} else if len(b) < len(a) && IntIsZeros(a[len(b):]) {
			a = a[:len(b)]
		} else {
			return false
		}
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func IntIsZeros(a []int) bool {
	for i := 0; i < len(a); i++ {
		if a[i] != 0 {
			return false
		}
	}
	return true
}

func IntDigitInc(dig []int, base int) {
	dig[0]++
	for i := 0; i < len(dig); i++ {
		c := 0
		for dig[i] >= base {
			dig[i] -= base
			c++
		}
		if i != len(dig)-1 {
			dig[i+1] += c
		}
	}
}

func BenchmarkGame(b *testing.B) {
	winCh := make(chan int, 1)
	plays := make([]int, 625)
	for i := 0; i < 625; i++ {
		plays[i] = i
	}
	bd := new(board)
	idx := make([]int, 4)
	lns := make([]lines.Line, 40)
	for i := 0; i < b.N; i++ {
		rand.Seed(1)
		game(plays, bd, idx, lns, winCh)
		<-winCh
	}
}
