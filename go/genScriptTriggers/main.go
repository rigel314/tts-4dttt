package main

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
)

func main() {
	gob.Register(map[string]interface{}{})

	f1, err := os.Open(`save.json`)
	if err != nil {
		panic(err)
	}

	jdec := json.NewDecoder(f1)
	var j []interface{}
	err = jdec.Decode(&j)
	if err != nil {
		panic(err)
	}
	// jreal := j.(map[string]interface{})
	// log.Println(jreal)

	// 2202 -> xzyw indexes
	p2202_x := j[0].(map[string]interface{})["Transform"].(map[string]interface{})["posX"].(float64)
	p2202_z := j[0].(map[string]interface{})["Transform"].(map[string]interface{})["posZ"].(float64)
	p2202_y := j[0].(map[string]interface{})["Transform"].(map[string]interface{})["posY"].(float64)
	p3202_x := j[1].(map[string]interface{})["Transform"].(map[string]interface{})["posX"].(float64)
	p2102_z := j[2].(map[string]interface{})["Transform"].(map[string]interface{})["posZ"].(float64)
	p2212_y := j[3].(map[string]interface{})["Transform"].(map[string]interface{})["posY"].(float64)
	p2203_x := j[4].(map[string]interface{})["Transform"].(map[string]interface{})["posX"].(float64)

	diffx := p3202_x - p2202_x
	diffz := p2202_z - p2102_z
	diffy := p2212_y - p2202_y
	diffw := p2203_x - p2202_x

	p0000_x := p2202_x - diffx*2 - diffw*2
	p0000_y := p2202_y
	p0000_z := p2202_z - diffz*2

	log.Println(diffx, diffz, diffy, diffw)

	jout := make([]interface{}, 0, 625)
	for w := 0; w < 5; w++ {
		for y := 0; y < 5; y++ {
			for z := 2; z < 3; z++ {
				for x := 2; x < 3; x++ {
					// copy j[0]
					buf := new(bytes.Buffer)
					ge := gob.NewEncoder(buf)
					err = ge.Encode(j[0].(map[string]interface{}))
					if err != nil {
						panic(err)
					}
					gd := gob.NewDecoder(bytes.NewReader(buf.Bytes()))
					var newj map[string]interface{}
					err = gd.Decode(&newj)
					if err != nil {
						panic(err)
					}

					newj["Transform"].(map[string]interface{})["scaleX"] = 6
					newj["Transform"].(map[string]interface{})["scaleZ"] = 6
					newj["Transform"].(map[string]interface{})["scaleY"] = .1
					newj["Transform"].(map[string]interface{})["posX"] = p0000_x + diffx*float64(x) + diffw*float64(w)
					newj["Transform"].(map[string]interface{})["posZ"] = p0000_z + diffz*float64(z)
					newj["Transform"].(map[string]interface{})["posY"] = p0000_y + diffy*float64(y)
					newj["LuaScript"] = fmt.Sprintf("x = %d\nz = %d\ny = %d\nw = %d\n", x, z, y, w)
					newj["Nickname"] = fmt.Sprintf(`st%d%d%d%d`, x, z, y, w)
					newj["GUID"] = fmt.Sprintf("%06x", rand.Intn(1<<24-1))

					jout = append(jout, newj)
				}
			}
		}
	}

	f2, err := os.Create(`save2.json`)
	if err != nil {
		panic(err)
	}
	jenc := json.NewEncoder(&unix2dosWriter{f2})
	jenc.SetIndent("", "  ")
	err = jenc.Encode(jout)
	if err != nil {
		panic(err)
	}
}

type unix2dosWriter struct{ f io.Writer }

func (uw *unix2dosWriter) Write(buf []byte) (int, error) {
	inner := make([]byte, len(buf))
	copy(inner, buf)
	inner = bytes.ReplaceAll(inner, []byte("\n"), []byte("\r\n"))
	n, err := uw.f.Write(inner)
	if n > len(buf) {
		n = len(buf) // Yes, I realize that this masks the case where \r is written, but not a \n
	}
	return n, err
}
