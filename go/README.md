go
======

## genScriptTriggers
This reads in some manually placed script triggers, finds the diffs in positions, then autogenerates script triggers for many places.

Originally, I intended to have a script trigger for each of the 625 playable areas, with each one storing [x,z,y,w].  But this seemed to make tabletop sim lag.  My solution was to only have one trigger for each 5x5 board, storing [y,w], and to figure out [x,z] based on collision point.

## winAnalysis
Programmatically detecting wins is surprisingly hard.  Though, only in the "pure math" side of things.  With a list of line starting points and directions, it should be pretty simple to check all of them.  Or to just check the ones that intersect the most recent piece.

This program finds out that there are 888 possible lines.  It also has multiple implementations that find all of the lines that pass through a given point, returning a vector(start and direction) for each intersecting line.

## lines
A library used by winAnalysis that contains the actual implementations of the lines through a point functions.

## randomPlayStats
A program that simulates gameplay with 2 players playing randomly, to calculate random-play game length and win/loss/tie distribution.
