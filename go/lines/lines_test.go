package lines

import "testing"

func TestChoose2(t *testing.T) {
	l := choose2([]int{1, 2, 3, 4, 5})
	for _, v := range l {
		t.Log(v)
	}
}

func TestChoose(t *testing.T) {
	l := Choose([]int{1, 2, 3, 4, 5}, 3)
	for _, v := range l {
		t.Log(v)
	}
	t.Log(len(l))
}

func TestDirWithIdx(t *testing.T) {
	v := NewDirectionWithIdx([]int{2, 3}, []int{1, -1}, 4)
	t.Log(v)
}

func TestLineWithVec(t *testing.T) {
	s, d := []int{0, 4, 4, 0}, []int{0, -1, -1, 1}
	v := NewLineFromVector(s, d)
	for i := range s {
		s[i] = 0
		d[i] = 0
	}
	t.Log(s)
	t.Log(v)
}

// func TestLineWithIdx(t *testing.T) {
// 	v := NewLineWithIdx([]int{0, 1}, []int{0, 4}, []int{1, -1}, 4)
// 	t.Log(v)
// }

func TestLineWithIdxStartCopy(t *testing.T) {
	v := NewLineWithIdxStartCopy([]int{0, 1}, []int{0, 0, 0, 0}, []int{0, 0}, []int{1, 1}, 4)
	t.Log(v)
}

func TestDiagTest(t *testing.T) {
	v := DiagTest([]int{0, 0, 0, 0}, []int{0}, []int{0}, 4, nil, nil)
	t.Log(v)
}
