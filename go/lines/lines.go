// Package lines has functions for finding paths in an N-dimensional tic-tac-toe board
package lines

// Line represents a N-dimensional through a discrete, square N-space
type Line struct {
	Start []int
	Diff  []int
}

// ND finds all canonical lines that pass through the given point
func ND(p []int) (ret []Line) {
	M := len(p)
	for d := 1; d <= M; d++ {
		l := Choose(p, d)
		idx := Choose([]int{0, 1, 2, 3}, d)
		for i, l := range l {
			tmp := make([]int, M)
			copy(tmp, p)
			// log.Println(d, i)
			ret = append(ret, DiagTest(p, l, idx[i], M, nil, nil)...)
		}
	}

	return
}

// Lines4DLooper is a 4-D specialized function that has the same output as ND
func Lines4DLooper(x, z, y, w int) (ret []Line) {
	M := 4
	_ = M

	ret = append(ret, Line{Start: []int{0, z, y, w}, Diff: []int{1, 0, 0, 0}})
	ret = append(ret, Line{Start: []int{x, 0, y, w}, Diff: []int{0, 1, 0, 0}})
	ret = append(ret, Line{Start: []int{x, z, 0, w}, Diff: []int{0, 0, 1, 0}})
	ret = append(ret, Line{Start: []int{x, z, y, 0}, Diff: []int{0, 0, 0, 1}})

	l2 := Choose([]int{x, z, y, w}, 2)
	idx2 := Choose([]int{0, 1, 2, 3}, 2)
	for i, l := range l2 {
		p := []int{x, z, y, w}
		ret = append(ret, DiagTest(p, l, idx2[i], M, nil, nil)...)
	}

	l3 := Choose([]int{x, z, y, w}, 3)
	idx3 := Choose([]int{0, 1, 2, 3}, 3)
	for i, l := range l3 {
		p := []int{x, z, y, w}
		ret = append(ret, DiagTest(p, l, idx3[i], M, nil, nil)...)
	}

	l4 := Choose([]int{x, z, y, w}, 4)
	idx4 := Choose([]int{0, 1, 2, 3}, 4)
	for i, l := range l4 {
		// 4D diagonals
		p := []int{x, z, y, w}
		ret = append(ret, DiagTest(p, l, idx4[i], M, nil, nil)...)
	}

	return
}

// Lines4DLoop is a 4-D specialized function that has the same output as ND
func Lines4DLoop(x, z, y, w int) (ret []Line) {
	M := 4

	ret = append(ret, Line{Start: []int{0, z, y, w}, Diff: []int{1, 0, 0, 0}})
	ret = append(ret, Line{Start: []int{x, 0, y, w}, Diff: []int{0, 1, 0, 0}})
	ret = append(ret, Line{Start: []int{x, z, 0, w}, Diff: []int{0, 0, 1, 0}})
	ret = append(ret, Line{Start: []int{x, z, y, 0}, Diff: []int{0, 0, 0, 1}})

	l2 := Choose([]int{x, z, y, w}, 2)
	idx2 := Choose([]int{0, 1, 2, 3}, 2)
	for i, v := range l2 {
		p := []int{x, z, y, w}
		if v[0] == v[1] {
			ret = append(ret, NewLineWithIdxStartCopy(idx2[i], p, []int{0, 0}, []int{1, 1}, M))
		}
		if v[0] == M-v[1] {
			ret = append(ret, NewLineWithIdxStartCopy(idx2[i], p, []int{0, M}, []int{1, -1}, M))
		}
	}

	l3 := Choose([]int{x, z, y, w}, 3)
	idx3 := Choose([]int{0, 1, 2, 3}, 3)
	for i, v := range l3 {
		p := []int{x, z, y, w}
		if v[0] == v[1] && v[0] == v[2] && v[1] == v[2] {
			ret = append(ret, NewLineWithIdxStartCopy(idx3[i], p, []int{0, 0, 0}, []int{1, 1, 1}, M))
		}
		if v[0] == M-v[1] && v[0] == v[2] && M-v[1] == v[2] {
			ret = append(ret, NewLineWithIdxStartCopy(idx3[i], p, []int{0, M, 0}, []int{1, -1, 1}, M))
		}
		if v[0] == v[1] && v[0] == M-v[2] && v[1] == M-v[2] {
			ret = append(ret, NewLineWithIdxStartCopy(idx3[i], p, []int{0, 0, M}, []int{1, 1, -1}, M))
		}
		if v[0] == M-v[1] && v[0] == M-v[2] && M-v[1] == M-v[2] {
			ret = append(ret, NewLineWithIdxStartCopy(idx3[i], p, []int{0, M, M}, []int{1, -1, -1}, M))
		}
	}

	// 4D diagonals
	if x == z && x == y && x == w && z == y && z == w && y == w { // x == z == y == w
		ret = append(ret, Line{Start: []int{0, 0, 0, 0}, Diff: []int{1, 1, 1, 1}})
	}
	if x == z && x == y && x == M-w && z == y && z == M-w && y == M-w { // x == z == y == M-w
		ret = append(ret, Line{Start: []int{0, 0, 0, M}, Diff: []int{1, 1, 1, -1}})
	}
	if x == z && x == M-y && x == w && z == M-y && z == w && M-y == w { // x == z == M-y == w
		ret = append(ret, Line{Start: []int{0, 0, M, 0}, Diff: []int{1, 1, -1, 1}})
	}
	if x == M-z && x == y && x == w && M-z == y && M-z == w && y == w { // x == M-z == y == w
		ret = append(ret, Line{Start: []int{0, M, 0, 0}, Diff: []int{1, -1, 1, 1}})
	}
	if x == z && x == M-y && x == M-w && z == M-y && z == M-w && M-y == M-w { // x == z == M-y == M-w
		ret = append(ret, Line{Start: []int{0, 0, M, M}, Diff: []int{1, 1, -1, -1}})
	}
	if x == M-z && x == M-y && x == w && M-z == M-y && M-z == w && M-y == w { // x == M-z == M-y == w
		ret = append(ret, Line{Start: []int{0, M, M, 0}, Diff: []int{1, -1, -1, 1}})
	}
	if x == M-z && x == y && x == M-w && M-z == y && M-z == M-w && y == M-w { // x == M-z == y == M-w
		ret = append(ret, Line{Start: []int{0, M, 0, M}, Diff: []int{1, -1, 1, -1}})
	}
	if x == M-z && x == M-y && x == M-w && M-z == M-y && M-z == M-w && M-y == M-w { // x == M-z == M-y == M-w
		ret = append(ret, Line{Start: []int{0, M, M, M}, Diff: []int{1, -1, -1, -1}})
	}

	return
}

// Lines4D is a fully unwrapped, 4-D specialized function that has the same output as ND
func Lines4D(x, z, y, w int) (ret []Line) {
	M := 4

	// 1D lines
	ret = append(ret, Line{Start: []int{0, z, y, w}, Diff: []int{1, 0, 0, 0}})
	ret = append(ret, Line{Start: []int{x, 0, y, w}, Diff: []int{0, 1, 0, 0}})
	ret = append(ret, Line{Start: []int{x, z, 0, w}, Diff: []int{0, 0, 1, 0}})
	ret = append(ret, Line{Start: []int{x, z, y, 0}, Diff: []int{0, 0, 0, 1}})

	// 2D diagonals, some points can have both (eg centers)
	if x == z {
		ret = append(ret, Line{Start: []int{0, 0, y, w}, Diff: []int{1, 1, 0, 0}})
	}
	if x == M-z {
		ret = append(ret, Line{Start: []int{0, M, y, w}, Diff: []int{1, -1, 0, 0}})
	}

	if x == y {
		ret = append(ret, Line{Start: []int{0, z, 0, w}, Diff: []int{1, 0, 1, 0}})
	}
	if x == M-y {
		ret = append(ret, Line{Start: []int{0, z, M, w}, Diff: []int{1, 0, -1, 0}})
	}

	if x == w {
		ret = append(ret, Line{Start: []int{0, z, y, 0}, Diff: []int{1, 0, 0, 1}})
	}
	if x == M-w {
		ret = append(ret, Line{Start: []int{0, z, y, M}, Diff: []int{1, 0, 0, -1}})
	}

	if z == y {
		ret = append(ret, Line{Start: []int{x, 0, 0, w}, Diff: []int{0, 1, 1, 0}})
	}
	if z == M-y {
		ret = append(ret, Line{Start: []int{x, 0, M, w}, Diff: []int{0, 1, -1, 0}})
	}

	if z == w {
		ret = append(ret, Line{Start: []int{x, 0, y, 0}, Diff: []int{0, 1, 0, 1}})
	}
	if z == M-w {
		ret = append(ret, Line{Start: []int{x, 0, y, M}, Diff: []int{0, 1, 0, -1}})
	}

	if y == w {
		ret = append(ret, Line{Start: []int{x, z, 0, 0}, Diff: []int{0, 0, 1, 1}})
	}
	if y == M-w {
		ret = append(ret, Line{Start: []int{x, z, 0, M}, Diff: []int{0, 0, 1, -1}})
	}

	// 3D diagonals
	if x == z && x == y && z == y { // x == z == y
		ret = append(ret, Line{Start: []int{0, 0, 0, w}, Diff: []int{1, 1, 1, 0}})
	}
	if x == M-z && x == y && M-z == y { // x == M-z == y
		ret = append(ret, Line{Start: []int{0, M, 0, w}, Diff: []int{1, -1, 1, 0}})
	}
	if x == z && x == M-y && z == M-y { // x == z == M-y
		ret = append(ret, Line{Start: []int{0, 0, M, w}, Diff: []int{1, 1, -1, 0}})
	}
	if x == M-z && x == M-y && M-z == M-y { // x == M-z == M-y
		ret = append(ret, Line{Start: []int{0, M, M, w}, Diff: []int{1, -1, -1, 0}})
	}

	if x == z && x == w && z == w { // x == z == w
		ret = append(ret, Line{Start: []int{0, 0, y, 0}, Diff: []int{1, 1, 0, 1}})
	}
	if x == M-z && x == w && M-z == w { // x == M-z == w
		ret = append(ret, Line{Start: []int{0, M, y, 0}, Diff: []int{1, -1, 0, 1}})
	}
	if x == z && x == M-w && z == M-w { // x == z == M-w
		ret = append(ret, Line{Start: []int{0, 0, y, M}, Diff: []int{1, 1, 0, -1}})
	}
	if x == M-z && x == M-w && M-z == M-w { // x == M-z == M-w
		ret = append(ret, Line{Start: []int{0, M, y, M}, Diff: []int{1, -1, 0, -1}})
	}

	if x == y && x == w && y == w { // x == y == w
		ret = append(ret, Line{Start: []int{0, z, 0, 0}, Diff: []int{1, 0, 1, 1}})
	}
	if x == M-y && x == w && M-y == w { // x == M-y == w
		ret = append(ret, Line{Start: []int{0, z, M, 0}, Diff: []int{1, 0, -1, 1}})
	}
	if x == y && x == M-w && y == M-w { // x == y == M-w
		ret = append(ret, Line{Start: []int{0, z, 0, M}, Diff: []int{1, 0, 1, -1}})
	}
	if x == M-y && x == M-w && M-y == M-w { // x == M-y == M-w
		ret = append(ret, Line{Start: []int{0, z, M, M}, Diff: []int{1, 0, -1, -1}})
	}

	if z == y && z == w && y == w { // z == y == w
		ret = append(ret, Line{Start: []int{x, 0, 0, 0}, Diff: []int{0, 1, 1, 1}})
	}
	if z == M-y && z == w && M-y == w { // z == M-y == w
		ret = append(ret, Line{Start: []int{x, 0, M, 0}, Diff: []int{0, 1, -1, 1}})
	}
	if z == y && z == M-w && y == M-w { // z == y == M-w
		ret = append(ret, Line{Start: []int{x, 0, 0, M}, Diff: []int{0, 1, 1, -1}})
	}
	if z == M-y && z == M-w && M-y == M-w { // z == M-y == M-w
		ret = append(ret, Line{Start: []int{x, 0, M, M}, Diff: []int{0, 1, -1, -1}})
	}

	// 4D diagonals
	if x == z && x == y && x == w && z == y && z == w && y == w { // x == z == y == w
		ret = append(ret, Line{Start: []int{0, 0, 0, 0}, Diff: []int{1, 1, 1, 1}})
	}
	if x == z && x == y && x == M-w && z == y && z == M-w && y == M-w { // x == z == y == M-w
		ret = append(ret, Line{Start: []int{0, 0, 0, M}, Diff: []int{1, 1, 1, -1}})
	}
	if x == z && x == M-y && x == w && z == M-y && z == w && M-y == w { // x == z == M-y == w
		ret = append(ret, Line{Start: []int{0, 0, M, 0}, Diff: []int{1, 1, -1, 1}})
	}
	if x == M-z && x == y && x == w && M-z == y && M-z == w && y == w { // x == M-z == y == w
		ret = append(ret, Line{Start: []int{0, M, 0, 0}, Diff: []int{1, -1, 1, 1}})
	}
	if x == z && x == M-y && x == M-w && z == M-y && z == M-w && M-y == M-w { // x == z == M-y == M-w
		ret = append(ret, Line{Start: []int{0, 0, M, M}, Diff: []int{1, 1, -1, -1}})
	}
	if x == M-z && x == M-y && x == w && M-z == M-y && M-z == w && M-y == w { // x == M-z == M-y == w
		ret = append(ret, Line{Start: []int{0, M, M, 0}, Diff: []int{1, -1, -1, 1}})
	}
	if x == M-z && x == y && x == M-w && M-z == y && M-z == M-w && y == M-w { // x == M-z == y == M-w
		ret = append(ret, Line{Start: []int{0, M, 0, M}, Diff: []int{1, -1, 1, -1}})
	}
	if x == M-z && x == M-y && x == M-w && M-z == M-y && M-z == M-w && M-y == M-w { // x == M-z == M-y == M-w
		ret = append(ret, Line{Start: []int{0, M, M, M}, Diff: []int{1, -1, -1, -1}})
	}

	return
}

// Lines4DPrealloc is a noallocation version of Lines4D
func Lines4DPrealloc(x, z, y, w int, l []Line) []Line {
	M := 4

	_ = l[39]

	// 1D lines
	_, _, _, _, _, _, _, _ = l[0].Start[3], l[1].Start[3], l[2].Start[3], l[3].Start[3], l[0].Diff[3], l[1].Diff[3], l[2].Diff[3], l[3].Diff[3]
	l[0].Start[0], l[0].Start[1], l[0].Start[2], l[0].Start[3], l[0].Diff[0], l[0].Diff[1], l[0].Diff[2], l[0].Diff[3] = 0, z, y, w, 1, 0, 0, 0
	l[1].Start[0], l[1].Start[1], l[1].Start[2], l[1].Start[3], l[1].Diff[0], l[1].Diff[1], l[1].Diff[2], l[1].Diff[3] = x, 0, y, w, 0, 1, 0, 0
	l[2].Start[0], l[2].Start[1], l[2].Start[2], l[2].Start[3], l[2].Diff[0], l[2].Diff[1], l[2].Diff[2], l[2].Diff[3] = x, z, 0, w, 0, 0, 1, 0
	l[3].Start[0], l[3].Start[1], l[3].Start[2], l[3].Start[3], l[3].Diff[0], l[3].Diff[1], l[3].Diff[2], l[3].Diff[3] = x, z, y, 0, 0, 0, 0, 1

	i := 4

	// 2D diagonals, some points can have both (eg centers)
	if x == z {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, y, w, 1, 1, 0, 0
		i++
	}
	if x == M-z {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, y, w, 1, -1, 0, 0
		i++
	}

	if x == y {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, 0, w, 1, 0, 1, 0
		i++
	}
	if x == M-y {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, M, w, 1, 0, -1, 0
		i++
	}

	if x == w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, y, 0, 1, 0, 0, 1
		i++
	}
	if x == M-w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, y, M, 1, 0, 0, -1
		i++
	}

	if z == y {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, 0, w, 0, 1, 1, 0
		i++
	}
	if z == M-y {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, M, w, 0, 1, -1, 0
		i++
	}

	if z == w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, y, 0, 0, 1, 0, 1
		i++
	}
	if z == M-w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, y, M, 0, 1, 0, -1
		i++
	}

	if y == w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, z, 0, 0, 0, 0, 1, 1
		i++
	}
	if y == M-w {
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, z, 0, M, 0, 0, 1, -1
		i++
	}

	// 3D diagonals
	if x == z && x == y && z == y { // x == z == y
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, 0, w, 1, 1, 1, 0
		i++
	}
	if x == M-z && x == y && M-z == y { // x == M-z == y
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, 0, w, 1, -1, 1, 0
		i++
	}
	if x == z && x == M-y && z == M-y { // x == z == M-y
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, M, w, 1, 1, -1, 0
		i++
	}
	if x == M-z && x == M-y && M-z == M-y { // x == M-z == M-y
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, M, w, 1, -1, -1, 0
		i++
	}

	if x == z && x == w && z == w { // x == z == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, y, 0, 1, 1, 0, 1
		i++
	}
	if x == M-z && x == w && M-z == w { // x == M-z == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, y, 0, 1, -1, 0, 1
		i++
	}
	if x == z && x == M-w && z == M-w { // x == z == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, y, M, 1, 1, 0, -1
		i++
	}
	if x == M-z && x == M-w && M-z == M-w { // x == M-z == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, y, M, 1, -1, 0, -1
		i++
	}

	if x == y && x == w && y == w { // x == y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, 0, 0, 1, 0, 1, 1
		i++
	}
	if x == M-y && x == w && M-y == w { // x == M-y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, M, 0, 1, 0, -1, 1
		i++
	}
	if x == y && x == M-w && y == M-w { // x == y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, 0, M, 1, 0, 1, -1
		i++
	}
	if x == M-y && x == M-w && M-y == M-w { // x == M-y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, z, M, M, 1, 0, -1, -1
		i++
	}

	if z == y && z == w && y == w { // z == y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, 0, 0, 0, 1, 1, 1
		i++
	}
	if z == M-y && z == w && M-y == w { // z == M-y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, M, 0, 0, 1, -1, 1
		i++
	}
	if z == y && z == M-w && y == M-w { // z == y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, 0, M, 0, 1, 1, -1
		i++
	}
	if z == M-y && z == M-w && M-y == M-w { // z == M-y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = x, 0, M, M, 0, 1, -1, -1
		i++
	}

	// 4D diagonals
	if x == z && x == y && x == w && z == y && z == w && y == w { // x == z == y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, 0, 0, 1, 1, 1, 1
		i++
	}
	if x == z && x == y && x == M-w && z == y && z == M-w && y == M-w { // x == z == y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, 0, M, 1, 1, 1, -1
		i++
	}
	if x == z && x == M-y && x == w && z == M-y && z == w && M-y == w { // x == z == M-y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, M, 0, 1, 1, -1, 1
		i++
	}
	if x == M-z && x == y && x == w && M-z == y && M-z == w && y == w { // x == M-z == y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, 0, 0, 1, -1, 1, 1
		i++
	}
	if x == z && x == M-y && x == M-w && z == M-y && z == M-w && M-y == M-w { // x == z == M-y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, 0, M, M, 1, 1, -1, -1
		i++
	}
	if x == M-z && x == M-y && x == w && M-z == M-y && M-z == w && M-y == w { // x == M-z == M-y == w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, M, 0, 1, -1, -1, 1
		i++
	}
	if x == M-z && x == y && x == M-w && M-z == y && M-z == M-w && y == M-w { // x == M-z == y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, 0, M, 1, -1, 1, -1
		i++
	}
	if x == M-z && x == M-y && x == M-w && M-z == M-y && M-z == M-w && M-y == M-w { // x == M-z == M-y == M-w
		_, _ = l[i].Start[3], l[i].Diff[3]
		l[i].Start[0], l[i].Start[1], l[i].Start[2], l[i].Start[3], l[i].Diff[0], l[i].Diff[1], l[i].Diff[2], l[i].Diff[3] = 0, M, M, M, 1, -1, -1, -1
		i++
	}

	return l[:i]
}

// Equal checks if two lines are equal
func (il *Line) Equal(l Line) bool {
	if len(il.Start) != len(l.Start) {
		return false
	}
	if len(il.Diff) != len(l.Diff) {
		return false
	}
	for i := 0; i < len(il.Start); i++ {
		if il.Start[i] != l.Start[i] {
			return false
		}
	}
	for i := 0; i < len(il.Diff); i++ {
		if il.Diff[i] != l.Diff[i] {
			return false
		}
	}
	return true
}

// Less checks if a given line is less than the receiver
func (il *Line) Less(l Line) bool {
	if len(il.Start) != len(l.Start) {
		panic("Less on different lengths")
	}
	if len(il.Diff) != len(l.Diff) {
		panic("Less on different lengths")
	}
	equal := true
	for i := 0; i < len(il.Start); i++ {
		if il.Start[i] > l.Start[i] {
			return false
		}
		if il.Start[i] != l.Start[i] {
			equal = false
			return true
		}
	}
	for i := 0; i < len(il.Diff); i++ {
		if il.Diff[i] > l.Diff[i] {
			return false
		}
		if il.Diff[i] != l.Diff[i] {
			equal = false
			return true
		}
	}
	if equal {
		return false
	}
	return true
}

// NewLineFromVector creates a new line from a given point and direction
func NewLineFromVector(p []int, d []int) (ret Line) {
	ret.Start = append(ret.Start, p...)
	ret.Diff = append(ret.Diff, d...)
	return
}

// // NewLineWithIdx
// func NewLineWithIdx(idx []int, p []int, d []int, dim int) (ret Line) {
// 	ret.Start = make([]int, dim)
// 	for i, v := range idx {
// 		ret.Start[v] = p[i]
// 	}
// 	ret.Diff = NewDirectionWithIdx(idx, d, dim)
// 	return ret
// }

// // NewLineWithIdxStart
// func NewLineWithIdxStart(idx []int, s []int, p []int, d []int, dim int) (ret Line) {
// 	ret.Start = s
// 	for i, v := range idx {
// 		ret.Start[v] = p[i]
// 	}
// 	ret.Diff = NewDirectionWithIdx(idx, d, dim)
// 	return ret
// }

// NewLineWithIdxStartCopy returns a new line with dim dimensionality, a copy of the given start, replacing elements specified in idx with corresponding values from point, with specified nonzero directions specified in dir
func NewLineWithIdxStartCopy(idx []int, s []int, p []int, d []int, dim int) (ret Line) {
	ret.Start = make([]int, dim)
	copy(ret.Start, s)
	for i, v := range idx {
		ret.Start[v] = p[i]
	}
	ret.Diff = NewDirectionWithIdx(idx, d, dim)
	return ret
}

// NewDirectionWithIdx returns a new direction array, of length dim, with nonzero indices and values specified in idx and dir
func NewDirectionWithIdx(idx []int, dir []int, dim int) []int {
	ret := make([]int, dim)
	for i, v := range idx {
		ret[v] = dir[i]
	}
	return ret
}

func max(a []int) int {
	var m int
	for i, e := range a {
		if i == 0 || e < m {
			m = e
		}
	}
	return m
}

// Choose returns a slice of int slices where each entry is a unique choice without replacement from a choose n
func Choose(a []int, n int) [][]int {
	var ret [][]int

	switch n {
	case 0:
		return ret
	case 1:
		for _, v := range a {
			ret = append(ret, []int{v})
		}
		return ret
	case 2:
		return choose2(a)
	case len(a):
		tmp := make([]int, n)
		copy(tmp, a)
		return [][]int{tmp}
	}

	for start := 0; start < len(a)-n+1; start++ {
		tmp := Choose(a[start+1:], n-1)
		for _, v := range tmp {
			ret = append(ret, append([]int{a[start]}, v...))
		}
	}

	return ret
}

func choose2(a []int) [][]int {
	var ret [][]int

	for i := 0; i < len(a)-1; i++ {
		for j := i + 1; j < len(a); j++ {
			ret = append(ret, []int{a[i], a[j]})
		}
	}

	return ret
}

// DiagTest returns a slice of all lines that pass through point in the dimensions specified by idx, this is a recursive function, so start and dir should be nil at the top level.
func DiagTest(p []int, l []int, idx []int, M int, s []int, d []int) (ret []Line) {
	switch len(l) {
	case 1:
		ret = append(ret, NewLineWithIdxStartCopy(idx, p, append([]int{0}, s...), append([]int{1}, d...), M))
		return
	case 2:
		if l[0] == l[1] {
			ret = append(ret, NewLineWithIdxStartCopy(idx, p, append([]int{0, 1}, s...), append([]int{1, 1}, d...), M))
		}
		if l[0] == M-l[1] {
			ret = append(ret, NewLineWithIdxStartCopy(idx, p, append([]int{0, M}, s...), append([]int{1, -1}, d...), M))
		}
		return
	}

	if l[0] == l[1] {
		ret = append(ret, DiagTest(p, l[1:], idx, M, append(s, 1), append(d, 1))...)
	}
	if l[0] == M-l[1] {
		ret = append(ret, DiagTest(p, l[1:], idx, M, append(s, M), append(d, -1))...)
	}

	return
}
