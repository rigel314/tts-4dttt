package main

import (
	"log"
	"sort"

	"gitlab.com/rigel314/tts-4dttt/go/lines"
)

func main() {
	// log.Fatal((&line{start: []int{3, 0, 0, 0}, diff: []int{0, 1, 0, 0}}).Less(line{start: []int{0, 4, 0, 0}, diff: []int{1, 0, 0, 0}}))
	// log.Fatal(lines4D(2, 2, 2, 2))

	log.Println("\nlines.Lines4D")
	testfunc(lines.Lines4D)
	log.Println("\nlines.Lines4DLoop")
	testfunc(lines.Lines4DLoop)
	log.Println("\nlines.Lines4DLooper")
	testfunc(lines.Lines4DLooper)
	log.Println("\nlines.LinesND")
	testfunc(func(x, z, y, w int) (ret []lines.Line) {
		return lines.LinesND([]int{x, z, y, w})
	})
}

func testfunc(f func(x, z, y, w int) (ret []lines.Line)) {
	var ls []lines.Line

	max := 0
	maxDims := []int{}
	for w := 0; w < 5; w++ {
		for y := 0; y < 5; y++ {
			for z := 0; z < 5; z++ {
				for x := 0; x < 5; x++ {
					thisls := f(x, z, y, w)
					if len(thisls) > max {
						max = len(thisls)
						maxDims = []int{x, z, y, w}
					}
					ls = append(ls, thisls...)
				}
			}
		}
	}

	sort.Slice(ls, func(i, j int) bool {
		return ls[i].Less(ls[j])
	})

	// log.Println("sorted")
	// for _, v := range ls {
	// 	fmt.Println(v)
	// }

	log.Println("max lines", max, ", point", maxDims)
	log.Println("total lines, including duplicates", len(ls))

	var uniq []lines.Line
	uniq = append(uniq, ls[0])
	last := ls[0]
	// log.Println(last)
	for _, v := range ls {
		if !v.Equal(last) {
			uniq = append(uniq, v)
			last = v
			// log.Println(v)
		}
	}

	log.Println("unique lines", len(uniq))
}
